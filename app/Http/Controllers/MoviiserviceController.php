<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Http;

class MoviiserviceController extends Controller
{
    public function index(Request $request){
        //Consulta de servicio Json
        $moviiservices = Storage::get('moviiservices.json');
        $dataServices = json_decode(($moviiservices), true);

        //Resultado de consulta del mes
        $result = Storage::get('result.json'); 
        $dataResult = json_decode(($result), true);
        $limite=5; //Cantidad de registros a mostrar
        
        $myCollectionObj = collect($dataResult);
        $dataResult = $this->paginate($myCollectionObj,$limite);
           
        return view('welcome', compact('dataResult','dataServices'));
    }
    

    public function consultaPorFechas(Request $request){
        //Consulta de servicio Json
        $moviiservice = Storage::get('result.json');
        $datalist = json_decode(($moviiservice), true);
       
        //Se asignan datos que llegan por post
        $servicio = $request->get('servicio');
        $fechaIni = $request->get('fechaInicio');
        $fechaFin = $request->get('fechaFin');

        //Validación de campos input
        request()->validate([
            'fechaInicio' => 'required',
            'fechaFin' => 'required',
            'servicio' => 'required'
        ]);

        $moviiservices = Storage::get('moviiservices.json');
        $dataServices = json_decode(($moviiservices), true);

        //Resultado de consulta del mes
        $result = Storage::get('result.json');
        $dataResult = json_decode(($result), true);
        $limite=5;
        
        //Se convierte el objeto a colección para poder procesarlo como paginador
        $myCollectionObj = collect($dataResult); 

        //Procesado de las fechas
        $dataResult1 = $myCollectionObj->filter(function ($value, $key) {
            
            $optdate1=explode(' ',$_POST['fechaInicio']);
            $optdate2=explode(' ',$_POST['fechaFin']);

            $i1= date("Y-m-d H:i:s:v", strtotime($optdate1[0]));
            $f1= date("Y-m-d H:i:s:v", strtotime($optdate2[0]));
            
            return (data_get($value, 'serviceSubType') == $_POST['servicio'] && (date("Y-m-d H:i:s:v", strtotime(data_get($value, 'transactionDate')))>= $i1 && date("Y-m-d H:i:s:v", strtotime(data_get($value, 'transactionDate')))<= $f1));
        });

        //$dataResult->$myCollectionObj->all();
        $dataResult = $this->paginate($dataResult1,$limite);
        return view('welcome', compact('dataResult','dataServices')); 
    }


    public function moviiservice(){
        //Consulta de servicio Json
        $moviiservices = Storage::get('moviiservices.json');
        $dataServices = json_decode(($moviiservices), true);
        
        //Resultado de consulta del mes
        $result = Storage::get('result.json'); 
        $dataResult = json_decode(($result), true);

        return $result; //Presenta los objetos que muestra la tabla de resultados
        //return $moviiservices; //Presenta los servicios que se muestran en el select 
    }


    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

}
