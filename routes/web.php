<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MoviiserviceController;

Route::get('/', [MoviiserviceController::class, 'index'])->name('moviiservice.index');
Route::get('/moviiservice', [MoviiserviceController::class, 'moviiservice'])->name('moviiservice.service');
Route::post('/', [MoviiserviceController::class, 'consultaPorFechas'])->name('moviiservice.consulta'); //pendiente

Route::get('/paginate', [MoviiserviceController::class, 'paginate'])->name('moviiservice.pagi');//sin funcionar
