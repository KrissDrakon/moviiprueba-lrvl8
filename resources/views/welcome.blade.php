<!DOCTYPE html>
<html>
<head>
  <title>Prueba Moviired-Laravel</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css"/>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
</head>
<body>
  <div class="container-fluid">
    <div class="card my-4">
      <div class="card-header">
      <h3>CONSULTA DE TRANSACCIONES DE SERVICIOS MOVIIRED</h3>
      </div>
      <div class="card-body">
        <p class="card-text">A continuación seleccione el servicio y rango de fechas:</p>
        <!-- Formulario de búsqueda -->
        <form method="POST">
          @csrf
          <div class="row">

            <div class="col-4 form-group">
              <select class="form-control" name="servicio" id="servicio">
                <option></option>
                @foreach($dataServices as $column)
                  <option value="{{ $column['code'] }}">{{ $column['name'] }} [{{ $column['code'] }}]</option>
                @endforeach
              </select>
              {!! $errors->first('servicio','<small>:message</small>') !!}
            </div>

            <div class="col-4 text-center">
              <input name="fechaInicio" class="form-control" type="date" placeholder="Ingresar fecha inicial" value="{{ old('fechaInicio') }}" >
              {!! $errors->first('fechaInicio','<small>:message</small>') !!}
            </div>

            <div class="col-4 text-center">
              <input name="fechaFin" class="form-control" type="date" placeholder="Ingresar fecha final" value="{{ old('fechaFin') }}">
              {!! $errors->first('fechaFin','<small>:message</small>') !!}
            </div>

            <div class="col-12 text-center py-4">              
              <button class="btn btn-primary w-100">REALIZAR CONSULTA</button>
            </div>
          </div>        
        </form>
                
        <!-- Tabla de contenido -->
        <table class="table table-hover table-condensed table-responsive-md">
          <thead>
            <tr>
              <th scope="col">CATEGORÍA</th>
              <th scope="col">SERVICIO</th>
              <th scope="col">REFERENCIA</th>
              <th scope="col">ID TRANSACCIÓN</th>
              <th scope="col">FECHA</th>
              <th scope="col">VALOR</th>
              <th scope="col">ESTADO</th>
            </tr>
          </thead>
          <tbody>
            @foreach($dataResult as $column)
              <tr>
                <td>{{ $column['serviceType'] }}</td>
                <td>{{ $column['name'] }}</td>
                <td>{{ $column['referenceNumber'] }}</td>
                <td>{{ $column['transferId'] }}</td>
                <td>{{ $column['transactionDate'] }}</td>
                <td>{{ $column['transferValue'] }}</td>
                <td>{{ $column['transferStatus'] }}</td>
              </tr>
            @endforeach
            <div id="paginator" style="width:inherit; float: right; ">
              {{ $dataResult->links('pagination::bootstrap-4') }}
            </div>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</body>
</html>